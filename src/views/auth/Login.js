import * as SecureStore from 'expo-secure-store';
import React, { useEffect, useState } from "react";
import { View } from "react-native";
import styled from "styled-components/native";
import { baseURL } from '../../../App';
import { Errors, SmallText, SmallTextHighlight } from '../../global.style';

export function Login({navigation}) {
  const [email, setEmail] = useState('v_pereira@outlook.fr');
  const [password, setPassword] = useState('vivileboss');
  const [error, setErrors] = useState(false);
  useEffect(() => {
    const jwt = async () => {
      const jwtKey = await SecureStore.getItemAsync('jwt');
      if (jwtKey) {
        navigation.navigate('Products')
      }
    }

    jwt();
  }, []);

    const login = async () => {
      const url = `${baseURL}api/prospect/login`;
      const options = {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({mail: email, password})
      };
      
      const response = await fetch(url, options);
      const data = await response.json();      
      const token = data.tokenJwt.toString()
      await SecureStore.setItemAsync('jwt', token);
      navigation.navigate('Products');
  }
  

  return (
    <Container>
      <CustomText>Connecte ton kawa</CustomText>
      <View>
        {error && (
          <Errors>Une erreur est survenue lors de la connexion</Errors>
        )}
        <CustomInput value={email} onChangeText={newText => setEmail(newText)} placeholder="Email" />
        <CustomInput secureTextEntry={true} value={password} onChangeText={newText => setPassword(newText)} placeholder="Mot de passe" />
      </View>

      <LoginButton
        title="Se connecter"
        onPress={login}
      />
      
      <View>
        <SmallText>Pas de compte ? <SmallTextHighlight onPress={() => {
          navigation.navigate('Register', {headerBackVisible: true});
        }}>inscrivez vous</SmallTextHighlight> </SmallText>

        <SmallText>Première connexion ? <SmallTextHighlight onPress={() => {
          navigation.navigate('ScanQrcode');
        }}>c'est ici</SmallTextHighlight> </SmallText>
      </View>
    </Container>
  )
}

const Container = styled.View`
  width: 100%;
  flex-grow: 1;
  align-items: center;
  justify-content: space-around;
`

const CustomText = styled.Text`
  color: #b07b2d;
  font-size: 32px;
  font-weight: bold;
`

const LoginButton = styled.Button`
  /* width: 200px;
  height: 60px;
  background-color: #b07b2d;
  color: lightgrey;
  font-weight: 700; */

`


const CustomInput = styled.TextInput`
  width: 220px;
  height: 32px;

  background-color: #f3f3f3;
  border: 1px solid #b07b2d;
  border-radius: 10px;

  color: black;
  font-size: 18px;
  padding: 5px;


  margin-top: 20px;
`
