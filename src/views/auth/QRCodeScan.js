import { BarCodeScanner } from 'expo-barcode-scanner';
import * as SecureStore from 'expo-secure-store';
import React, { useEffect, useState } from "react";
import { Button, View } from 'react-native';
import styled from "styled-components/native";
import { SmallText, SmallTextHighlight } from '../../global.style';

export function QRCodeScan({navigation}) {
  const [hashPermission, setHashPermission] = useState(null);
  const [scanned, setScanned] = useState(false);  
  const [text, setText] = useState('Not yet scanned');  

  const handleFirstConnexion = async (token) => {
    if(!token){
      setText('Ce qrcode ne respecte pas notre format');
      return;
    }
    
    await SecureStore.setItemAsync('jwt', token);
    navigation.navigate('Products')
  }

  const askForCameraPermission = () => {
    (async () => {
      const {status} = await BarCodeScanner.requestPermissionsAsync();
      setHashPermission(status === 'granted')
    })()
  }

  useEffect(() => {
    askForCameraPermission();
  }, [])

  const handleBarCodeScanned = ({type, data}) => {
    setScanned(true);
    const dataSerialize = JSON.parse(data);
    handleFirstConnexion(dataSerialize?.token)
  }

  if(hashPermission === null){
    return (
      <Container>
        <Text>Requesting for camera permission </Text>
      </Container>
    );
  }

  if(hashPermission === false){
    return (
      <Container>
        <Text>Nos access to camera</Text>
        {/* <CameraImageWrapper>
          <CameraImage source={require("assets/images/camera.png")} />
        </CameraImageWrapper> */}
        <Button title={'Allow Camera'} onPress={() => askForCameraPermission()} />
      </Container>
    );
  }
  
  return (
    <Container>

      <SmallText>
        Un compte ? 
        <SmallTextHighlight onPress={() => {
          navigation.navigate('Login');
        }}>
          connectez vous</SmallTextHighlight>
        </SmallText>
    
      <Text>Scanne ton kawa</Text>
      
      <View>
        <BarCodeScanner
          onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
          style={{ height: 400, width: 400 }} />
      </View>
      <Text>{text}</Text>
      {scanned && <Button title={'Scan again?'} onPress={() => setScanned(false)} color='tomato' />}
    </Container>
  )
}

const Container = styled.View`
  width: 100%;
  flex-grow: 1;
  align-items: center;
  justify-content: center;
`

const Text = styled.Text`
  color: #b07b2d;
  font-size: 32px;
  font-weight: bold;
`

const CameraImageWrapper = styled.View`
  width: 180px;
  height: 180px;
  padding: 40px;
  border: 4px solid #b07b2d;
  border-radius: 10px;
  margin-top: 30px;
`

const CameraImage = styled.Image`
  width: 100%;
  height: 100%;
`
