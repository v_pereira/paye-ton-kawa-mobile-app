import React, { useState } from "react";
import { View } from "react-native";
import styled from "styled-components/native";
import { baseURL } from '../../../App';
import { Errors, SmallText, SmallTextHighlight } from '../../global.style';


export function Register({navigation}) {
  const [firstname, setFirstname] = useState('Test');
  const [lastname, setLastname] = useState('Test');
  const [company, setCompany] = useState('EPSI');
  const [email, setEmail] = useState('sidol51615@fenwazi.com');
  const [password, setPassword] = useState('Babinks34.');
  const [error, setErrors] = useState(false);

  const register = async () => {
    try {
      const res = await fetch(`${baseURL}api/prospect/register`, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        mode: 'cors',
        body: JSON.stringify({
          name: firstname,
          lastname,
          company,
          mail: email, 
          password
        }),
      })
      console.log(await res.json());
      setErrors(false);
      navigation.navigate('ScanQrcode');
    }
    catch(error) {
      setErrors(true);
      console.error(error);
    }
  }

  return (
    <Container>

      <Text>Connecte ton kawa</Text>

      <View>
        {error && (
          <Errors>Une erreur est survenue lors de l'inscription</Errors>
        )}
        <CustomInput value={firstname} onChangeText={newText => setFirstname(newText)} placeholder="Prénom" />
        <CustomInput value={lastname} onChangeText={newText => setLastname(newText)} placeholder="Nom" />
        <CustomInput value={company} onChangeText={newText => setCompany(newText)} placeholder="Société" />
        <CustomInput value={email} onChangeText={newText => setEmail(newText)} placeholder="Email" />
        <CustomInput secureTextEntry={true} value={password} onChangeText={newText => setPassword(newText)} placeholder="Mot de passe" />
      </View>

      <RegisterButton
        onPress={register}
        title="S'inscrire"
      />

      <SmallText>Un compte ? <SmallTextHighlight onPress={() => {
        navigation.navigate('Login');
      }}>connectez vous</SmallTextHighlight> </SmallText>

    </Container>
  )
}

const Container = styled.View`
  width: 100%;
  flex-grow: 1;
  align-items: center;
  justify-content: space-around;
`

const Text = styled.Text`
  color: #b07b2d;
  font-size: 32px;
  font-weight: bold;
`

const RegisterButton = styled.Button`
  /* width: 200px;
  height: 60px;
  background-color: #b07b2d;
  color: lightgrey;
  font-weight: 700; */

`


const CustomInput = styled.TextInput`
  width: 220px;
  height: 32px;

  background-color: #f3f3f3;
  border: 1px solid #b07b2d;
  border-radius: 10px;

  color: black;
  font-size: 18px;
  padding: 5px;


  margin-top: 20px;
`
