import * as SecureStore from 'expo-secure-store';
import React, { useEffect, useState } from "react";
import { Text, TouchableHighlight, View } from "react-native";
import styled from "styled-components/native";
import { baseURL } from '../../../App';
import { Errors } from '../../global.style';



export function Products({navigation}) {
  const [kawas, setKawas] = useState([]);
  const [errors, setErros] = useState('');

  useEffect(() => {

    const getProducts = async () => {
      const jwt = await SecureStore.getItemAsync('jwt');
      const products = await SecureStore.getItemAsync('products');
      if (!jwt) navigation.navigate('Login');
      if(products) {
        setKawas(JSON.parse(products));
        return;
      };

        const res = await fetch(`${baseURL}api/prospect/products`, {
          method: 'GET',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'x-access-token': jwt
          },
          mode: 'cors'
        })
        const resJSON = await res.json();
        await SecureStore.setItemAsync('products', JSON.stringify(resJSON));
        setKawas(resJSON);
      
    }
    getProducts();
  }, []);
  
  return (
    <Container>

      {errors && (
        <Errors>Une erreur est survenue lors du chargement des produits</Errors>
      )}
      <CustomText>Nos kawas</CustomText>

      <KawaListScroll>
        {kawas && kawas.map((product, index) => (
          <TouchableHighlight onPress={() => { navigation.navigate('SpecificProduct', { product }) }}>
            <View key={`${index}-${product.name}`}>
              <KawaImage source={require("assets/images/kawa.jpg")} key={`${index}-${product.name}`}/>
              <Text>{product.name}</Text>
              <Text>{product.price}</Text>
              <Text>{product.stock}</Text>
            </View>
          </TouchableHighlight>
        ))}
      </KawaListScroll>

    </Container>
  )
}

const Container = styled.View`
  width: 100%;
  flex-grow: 1;
  align-items: center;
  justify-content: center;
  margin-top: 20px;
`

const CustomText = styled.Text`
  color: #b07b2d;
  font-size: 32px;
  font-weight: bold;
  margin-bottom: 20px;
`

const KawaListScroll = styled.ScrollView`
  width: 55%;
  height: 200px;  
`

const KawaImage = styled.Image`
  width: 205px;
  height: 205px;
  margin-bottom: 20px;
`
