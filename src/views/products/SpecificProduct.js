import React from "react";
import { TouchableHighlight } from 'react-native';
import styled from "styled-components/native";
import { hashType } from './DeepLinks';

export function SpecificProduct({route, navigation}) {
  
  // récupérer les données du specific product dans kawa
  const {product} = route.params;
  
  return (
    <Container>
      {/* REDIRECT VERS LA LISTE DE PRODUITS (Products.js) */}

      <CustomTouchableHighlight onPress={() => navigation.goBack()}>
        <ArrowImage source={require("assets/images/arrow.png")} />
      </CustomTouchableHighlight>
      
      <TouchableHighlight onPress={hashType[product.stock % 2 ? 'cube': 'capsule']}>
        <KawaImage source={require("assets/images/camera.png")} />
      </TouchableHighlight>

      <KawaContainer>
        <KawaSpecText>{product.name}</KawaSpecText>
        <KawaSpecText>Spec du kawa 1</KawaSpecText>
        <KawaSpecText>Spec du kawa 2</KawaSpecText>
        <KawaSpecText>{product.price}</KawaSpecText>
        <KawaSpecText>{product.stock}</KawaSpecText>
      </KawaContainer>
      <KawaSpecText>{product.description}</KawaSpecText>
    </Container>
  );
}

const Container = styled.View`
  width: 100%;
  flex-grow: 1;
  align-items: center;
  justify-content: space-around;
`

const ArrowImage = styled.Image`
  width: 45px;
  height: 45px; 
  margin-left: 20px;
`

// const Text = styled.Text`
//   color: #b07b2d;
//   font-size: 32px;
//   font-weight: bold;
// `

const KawaContainer = styled.View`
  width: 260px;
  height: 190px;
  border: 2px solid #b07b2d;
  border-radius: 10px;
  justify-content: center;
  /* align-items: center; */
  padding: 40px;
`

const KawaSpecText = styled.Text`
  font-size: 20px;
`

const KawaImage = styled.Image`
  width: 135px;
  height: 135px;
`

const CustomTouchableHighlight = styled.TouchableHighlight`
  width: 45px;
  height: 45px;
  align-self: flex-start;
  margin-left: 20px;
`;
