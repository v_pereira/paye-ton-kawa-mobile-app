import { Linking } from 'react-native';

// export function DeepLinkButtonCapsule() {

//   const openLinkCapsule = async () => {
//     const url = 'unitydl://mylink?gameObject=Capsule';
//     Linking.openURL(url);
//   };
//   return (
//     <View>
//       <Button title="Ouvrir le lien Capsule" onPress={openLinkCapsule} />
//     </View>
//   );
// };

// export function DeepLinkButtonCube() {
//   const openLinkCube = async () => {
//     const url = 'unitydl://mylink?gameObject=Cube';
//     Linking.openURL(url);
//   };
//   return (
//     <View>
//       <Button title="Ouvrir le lien Cube" onPress={openLinkCube} />
//     </View>
//   );
// };

const openLinkCapsule = async () => {
  const url = 'unitydl://mylink?gameObject=Capsule';
  Linking.openURL(url);
};

const openLinkCube = async () => {
  const url = 'unitydl://mylink?gameObject=Cube';
  Linking.openURL(url);
};

export const hashType = {
  cube: openLinkCube,
  capsule: openLinkCapsule,
};
