import styled from "styled-components/native";

export const Errors = styled.Text`
  width: 220px;
  color: red;
  text-align: center;
`

export const SmallTextHighlight = styled.Text`
  color: black;
`

export const SmallText = styled.Text`
  font-size: 12px;
  color: gray;
`