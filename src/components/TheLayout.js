import React from "react"
import styled from "styled-components";
import { SafeAreaView } from "react-native";

export function TheLayout(props) {
  return (
    <SafeAreaView>
      <CustomLayout>
        { props.children }
      </CustomLayout>
    </SafeAreaView>
  )
}

const CustomLayout = styled.View`
  height: 100%;
  align-items: center;
`
