import React from "react"
import styled from "styled-components/native";

export function TheHeader() {
  return (
    <Container>
      <LogoImage source={require('assets/images/paye-ton-kawa-poto.png')} />
    </Container>
  )
}

const Container = styled.View`
  width: 100%;
  height: 200px;
  display: flex;
  align-items: center;
  justify-content: flex-end;
`

const LogoImage = styled.Image`
  width: 180px;
  height: 180px;
`
