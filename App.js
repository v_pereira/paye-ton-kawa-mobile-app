import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import React from 'react';
import { Login } from './src/views/auth/Login';
import { QRCodeScan } from './src/views/auth/QRCodeScan';
import { Register } from './src/views/auth/Register';
import { Products } from './src/views/products/Products';
import { SpecificProduct } from './src/views/products/SpecificProduct';

const Stack = createNativeStackNavigator();
const options = {
  headerBackVisible: false,
  headerShown: false,
}
export const baseURL = "http://35.181.59.55:8000/";
const App = () => {
  return (
    <NavigationContainer>
        <Stack.Navigator initialRouteName='Login'>
          <Stack.Screen name="Login" component={Login} options={options}/>
          <Stack.Screen name="Register" component={Register} options={options}/>
          <Stack.Screen name="ScanQrcode" component={QRCodeScan} options={options}/>
          <Stack.Screen name="Products" component={Products} options={options}/>
          <Stack.Screen name="SpecificProduct" component={SpecificProduct} options={options}/>
        </Stack.Navigator>
    </NavigationContainer>
  )
};

export default App;